# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
s = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)
# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31004)

s.bind(server_address)
# Nasłuchiwanie przychodzących połączeń
s.listen(1)
while True:
    # Czekanie na połączenie
    connection, client_address = s.accept()
    print("Nawiazanie polaczenia")
    try:

        # Odebranie danych i odesłanie ich spowrotem
        response = connection.recv(128)
        print("Odebranie wiadomosci: " + response + " i wysylanie z powrotem")
        connection.sendall(response)
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
