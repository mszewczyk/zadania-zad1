# -*- encoding: utf-8 -*-

import socket

# Ustawienie licznika na zero
i = 0
# Tworzenie gniazda TCP/IP
s = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)
# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31004)
s.bind(server_address)
# Nasłuchiwanie przychodzących połączeń
s.listen(1)
while True:
    # Czekanie na połączenie
    connection, client_address = s.accept()
    # Podbicie licznika
    i += 1
    try:
        print("Nawiazanie polaczenia")
        # Wysłanie wartości licznika do klienta
        print("wysyłanie licznika")
        connection.sendall(str(i))
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
