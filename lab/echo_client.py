# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
s = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)
# Połączenie z gniazdem nasłuchującego serwera
server_address = ('194.29.175.240', 31004)

s.connect(server_address)
try:
    # Wysłanie danych
    message = 'To jest wiadomość, która zostanie zwrócona.'
    s.sendall(message)
    # Wypisanie odpowiedzi
    response = s.recv(128)
    print("Wiadomosc zwrotna od serwera: " + response)
finally:
    # Zamknięcie połączenia
    s.close()
    pass