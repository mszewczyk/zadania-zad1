# -*- encoding: utf-8 -*-


import socket
import exceptions


# Tworzenie gniazda TCP/IP
s = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)
# Połączenie z gniazdem nasłuchującego serwera
server_address = ('194.29.175.240', 31004) #194.29.175.240

s.connect(server_address)
try:
    my_input = '0'
# Wysłanie danych
    # user_input will be set to my_input if they just press enter
    while True:
        user_input1 = raw_input("Podaj 1 liczbe (domyślnie: %s):\n" % my_input) or my_input
        try:
            float(user_input1)
            break
        except exceptions.ValueError:
            print("Podany ciag znakow jest nieprawidlowy")
        pass
    print("Wysylanie liczby: " + user_input1 + " na serwer")
    s.sendall(user_input1)

    while True:
        user_input2 = raw_input("Podaj 2 liczbe (domyślnie: %s):\n" % my_input) or my_input
        try:
            float(user_input2)
            break
        except exceptions.ValueError:
            print("Podany ciag znakow jest nieprawidlowy")
        pass
    print("Wysylanie liczby: " + user_input2 + " na serwer")
    s.sendall(user_input2)
    # Wypisanie odpowiedzi
    response = s.recv(128)
    print("Wiadomosc zwrotna od serwera: " + response)
finally:
    # Zamknięcie połączenia
    s.close()
    print("Zamkniecie połączenia")
    pass