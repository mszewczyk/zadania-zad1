# -*- encoding: utf-8 -*-

import socket
import exceptions




# Tworzenie gniazda TCP/IP
s = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)
# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31004) #194.29.175.240

s.bind(server_address)
# Nasłuchiwanie przychodzących połączeń
s.listen(1)
while True:
    # Czekanie na połączenie
    connection, client_address = s.accept()
    print("Nawiazanie polaczenia")
    try:

        # Odebranie danych i odesłanie ich z powrotem

        while True:
            r1 = connection.recv(128)
            print("Odebranie pierwszej liczby: " + r1)
            r2 = connection.recv(128)
            print("Odebranie drugiej liczby: " + r2)
            try:
                sum = float(r1) + float(r2)
            finally:
                print("Wysylanie sumy: " + str(sum))
                connection.sendall(str(sum))
                break
            pass
    finally:
        # Zamknięcie połączenia
        connection.close()
        print("Zamkniecie polaczenia")
        pass


